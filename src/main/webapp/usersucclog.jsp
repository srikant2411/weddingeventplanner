<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Login</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<h4>Welcome ${username } ${user.userId}</h4>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="home">Home</a>

<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="requestquot">Request Quotation</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="/event/viewquot?userId=${user.userId}">View Quotation</a>

<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="userprofile">View Profile</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="userlogout">Logout</a>
<br>
<h4>${mess}</h4>
</body>
</html>