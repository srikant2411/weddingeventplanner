<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
           <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <style>
      #form_fname{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 230px;
         position: center;
         text-align: center;
      }
      #form_sname{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 230px;
         position: center;
         text-align: center;
      }
      #form_date{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 150px;
         position: center;
         text-align: center;
      }
      #validationCustom04{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 140px;
         position: center;
         text-align: center;
      }
      #form_contact{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 230px;
         position: center;
         text-align: center;
      }
      #form_password{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 230px;
         position: center;
         text-align: center;
      }
      #form_address{
         margin: 4px;
         border: 2px solid #ccc;
         border-radius: 4px;
         background-color: #f8f8f8;
         width: 230px;
         position: center;
         text-align: center;
      }
      #btn{
     background-color: #4CAF50;
     color: white;
     cursor:pointer;
     border: none;
     font-weight: bold;
     padding: 10px 20px;
     margin: 10px;
     border-radius: 15px;
     width: 215px; 
 }
 #userbox{
   background-position: center;
     height: 700px;
     position: relative;
     margin: auto;
     width: 50%;
     border: 4px solid black;
     background-color: rgb(245, 224, 178);
     background-size: cover;
     background-repeat: no-repeat;
     background-image: url("https://i.pinimg.com/originals/e6/f2/36/e6f236a7584f183cce3a4feb95e0e4d1.jpg");
     padding: 10px;
     border-radius: 10px;
     text-align:center;
     min-height: 10em;
     display: block;
 }
 
   </style>
</head>
<body>
${mes}
<%-- <forms:form action="userregdata" method="post"  modelAttribute="userdata">

<div class="form-group">

    <input type="hidden" class="form-control" name="userId" id="userId" placeholder="userId" value="default" required>
</div>
<div class="form-group">
 <label for="firstName">Enter the firstName</label>
    <input type="text" class="form-control" name="firstName" id="firstName" placeholder="firstName" required>
</div>

<div class="form-group">
 <label for="lastName">Enter the lastName</label>
    <input type="text" class="form-control" name="lastName" id="lastName" placeholder="lastName" required>
</div>
<div class="form-group">
 <label for="dob">Enter the Date of Birth</label>
    <input type="date" class="form-control" name="dob" id="dob" placeholder="dob" required>
</div>
<div class="form-group">
 <label for="gender">Enter the Date of gender</label>
    <input type="text" class="form-control" name="gender" id="gender" placeholder="gender" required>
</div>
<div class="form-group">
 <label for="contactno">Enter the Date of contactno</label>
    <input type="text" class="form-control" name="contactno" id="contactno" placeholder="contactno" required>
</div>
<div class="form-group">
 <label for="password">Enter the Date of password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="password" required>
</div>
<div class="form-group">
 <label for="address">Enter the Date of address</label>
    <input type="text" class="form-control" name="address" id="address" placeholder="address" required>
</div>
<div class="form-group">

    <input type="hidden" class="form-control" name="status" id="status" placeholder="status" value=1 required>
</div>

<input type="submit" class="btn btn-success" value="Register">
</forms:form> --%>
 <div id=userbox>
	<div class="container">
      <h2 style="color: rgb(148, 13, 143); font-style: italic;">_____ Register Here _____</h2>
      <img src="https://img.icons8.com/plasticine/2x/edit-user-male.png">
		<form:form action="userregdata" method="post"  modelAttribute="userdata" id="registration_form" >
		<div class="form-group">

    <input type="hidden" class="form-control" name="userId" id="userId" placeholder="userId" value="default" required>
</div>
			<div class="col-md-6">
            <input type="text" id="form_fname" name="firstName" required placeholder="Enter First Name">
            <br>
				<span class="error_form" id="fname_error_message" style="color: red; font-size: small;"></span>
				<!--Enter First Name-->	
			</div>
			<div class="col-md-6">
            <input type="text" id="form_sname" name="lastName" required placeholder="Enter Last Name">
            <br>
				<span class="error_form" id="sname_error_message" style="color: red; font-size: small;"></span>
				<!--Enter Last Name-->	
			</div>
			<div class="col-md-6">
            <label style="font-weight: 500;">Date of Birth </label>
            <input type="date" id="form_date" name="dob" required>
            <br>
				<span class="error_form" id="date_error_message" style="color: red; font-size: small;"></span>
				<!--Enter the Date of Birth-->
			</div>
			 <div class="col-md-6">
    <label for="validationCustom04" class="form-label" style="font-weight: 500;">Select Gender</label>
    <select class="form-select" id="validationCustom04" name="gender" required>
      <option selected disabled value="">Choose...</option>
      <option>Male</option>
      <option>Female</option>
      <option>Other</option>
    </select>
    <div class="invalid-feedback">
      Please select a valid option.
    </div>
  </div>
			<div class="col-md-6">
				<input type="text" id="form_contact" name="contactno" pattern="[0-9]{10}" required="" placeholder="Enter Contact Number">
            <br>
            <span class="error_form" id="contact_error_message" style="color: red; font-size: small;"></span>
				<!--Enter Contact Number-->
			</div>
			<div class="col-md-6">
				<input type="password" id="form_password" name="password" placeholder="Enter Password">
            <br>
            <span class="error_form" id="password_error_message" style="color: red; font-size: small;"></span>
				<!--Enter Password-->	
			</div>
			<div class="col-md-6">
				<input type="text" id="form_address" name="address" required placeholder="Enter Address">
            <br>
            <span class="error_form" id="address_error_message" style="color: red; font-size: small;"></span>
				<!--Enter Address-->
			</div>
			<br><br>
			 <div class="col-12">
			 <div class="form-group">

    <input type="hidden" class="form-control" name="status" id="status" placeholder="status" value=1 required>
</div>
    <input type="submit" value="Register" name="" id="btn">
	 </div>
			
		</form:form>
   </div>
   </div>
	<script type="text/javascript">
      $(function() {

         $("#fname_error_message").hide();
         $("#sname_error_message").hide();
		 $("#date_error_message").hide();
         $("#gender_error_message").hide();
		  $("#contact_error_message").hide();
         $("#password_error_message").hide();
		  $("#address_error_message").hide();
         

         var error_fname = false;
         var error_sname = false;
         var error_date = false;
		 var error_gender = false; 
		 var error_contact = false;
         var error_password = false;
		 var error_address = false;
         

         $("#form_fname").focusout(function(){
            check_fname();
         });
         $("#form_sname").focusout(function() {
            check_sname();
         });
         $("#form_date").focusout(function() {
            check_date();
         });
		 $("#form_gender").focusout(function(){
            check_gender();
         });
		 $("#form_contact").focusout(function() {
            check_contact();
         });
         $("#form_password").focusout(function() {
            check_password();
         });
		 
         $("#form_address").focusout(function() {
            check_address();
         });
		 

         function check_fname() {
            var pattern = /^[a-zA-Z]*$/;
            var fname = $("#form_fname").val();
            if (pattern.test(fname) && fname !== '') {
               $("#fname_error_message").hide();
               $("#form_fname").css("border-bottom","2px solid #34F458");
            } else {
               $("#fname_error_message").html(" *Should contain only Characters");
               $("#fname_error_message").show();
               $("#form_fname").css("border-bottom","2px solid #F90A0A");
               error_fname = true;
            }
         }
		 
	

         function check_sname() {
            var pattern = /^[a-zA-Z]*$/;
            var sname = $("#form_sname").val()
            if (pattern.test(sname) && sname !== '') {
               $("#sname_error_message").hide();
               $("#form_sname").css("border-bottom","2px solid #34F458");
            } else {
               $("#sname_error_message").html(" *Should contain only Characters");
               $("#sname_error_message").show();
               $("#form_sname").css("border-bottom","2px solid #F90A0A");
               error_fname = true;
            }
         }

         function check_address() {
            var pattern = /^[a-zA-Z0-9]*$/;
            var address = $("#form_address").val();
            if (pattern.test(address) && address !== '') {
               $("#address_error_message").hide();
               $("#form_address").css("border-bottom","2px solid #34F458");
            } else {
               $("#address_error_message").html(" *Enter correct address");
               $("#address_error_message").show();
               $("#form_address").css("border-bottom","2px solid #F90A0A");
               error_address = true;
            }
         }
         function check_password() {
            var password_length = $("#form_password").val().length;
            if (password_length < 6) {
               $("#password_error_message").html(" *Atleast 6 Characters");
               $("#password_error_message").show();
               $("#form_password").css("border-bottom","2px solid #F90A0A");
               error_password = true;
            } else {
               $("#password_error_message").hide();
               $("#form_password").css("border-bottom","2px solid #34F458");
            }
         }
		 
         function check_contact() {
            var contact_length = $("#form_contact").val().length;
            if (contact_length < 10) {
               $("#contact_error_message").html("Atleast 10 Digit");
               $("#contact_error_message").show();
               $("#form_contact").css("border-bottom","2px solid #F90A0A");
               error_contact = true;
            } else {
               $("#contact_error_message").hide();
               $("#form_contact").css("border-bottom","2px solid #34F458");
            }
         }
		 
		   function check_date(){
            var today = new Date();
            var dob = new Date($("#form_date").val());
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
			
               if(age < 18)
               {
               $("#date_error_message").html(" *Age should be greater than 18");
                  $("#date_error_message").show();
               $("#form_date").css("border-bottom","2px solid #F90A0A");
                  error_date=true;
               }
               else { 
               $("#date_error_message").hide();
                        $("#form_date").css("border-bottom","2px solid #34F458");
                     }
               }



         $("#registration_form").submit(function(){
		      console.log("ss");
            error_fname = false;
            error_sname = false;
            error_date = false;
			   error_contact = false;
            error_password = false;
            error_address = false;

            check_fname();
            check_sname();
            check_date();
						check_contact();
            check_password();
            check_address();

            if (error_fname === false && error_sname === false && error_date === false && error_contact === false && error_password === false && error_address === false) {
               alert("Registration Successfull");
			   console.log("sucessfull");
               return true;
            } else {
               alert("Please Fill the form Correctly");
			   console.log("not");
               return false;
            }


         });
      });
	  
   </script>
</body>
</html>