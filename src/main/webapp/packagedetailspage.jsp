<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Products</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
.material-icons {
					vertical-align: middle;
					}
	.link{ color: black; }			
	.link:hover { color: red; }
img{  
height: 300px;
  width: 500px;
}
</style>
</head>
<body>



<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="addpackage">Add New Package</a>

<div class="container-fluid">
			<h3>Products </h3>
			<div class="row">
				

<c:forEach items= "${vpackage}" var="pro">
<div class="col-md">
<div class="card">
<img class="card-img-top" src=${pro.photo }>
<div class="card-body">
<h5 class="card-title">${pro.packageName}</h5>
<p>Rs. ${pro.amount}  <i class="material-icons" style="color:#cc0000"></i>
<p class="card-text">${pro.packageDetails}</p>
<a class="btn btn-success" role="button" href="/event/quotouser?userId=${userId}&vendorId=${vendorId}&packageId=${pro.packageId}">Send To User</a>

</div>
</div>
</div>
</c:forEach>
</div>






		<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
	   
		
</div>
</body>
</html>