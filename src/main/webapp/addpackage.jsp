<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vendor Add Package</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
<forms:form action="insertpackage" method="post"  modelAttribute="upackage">
<div class="form-group">

<label for="vendorId">Your ID is </label>

 <input type="text" class="form-control" value="${vednorId}" disabled>
 <input type="hidden" class="form-control" name="vendorId" placeholder="vendorId" value="${vednorId}" >
 </div>
 
 
 <div class="form-group">

<label for="packageName">Enter The package Name</label>

 <input type="text" class="form-control" name="packageName" placeholder="packageName" required >
 </div>
 
 <div class="form-group">

<label for="packageDetails">Enter The package Details</label>

 <input type="text" class="form-control" name="packageDetails" placeholder="packageDetails" required >
 </div>
 
  <div class="form-group">

<label for="photo">Enter The Photo URL</label>

 <input type="text" class="form-control" name="photo" placeholder="photo" required >
 </div>
 
 <div class="form-group">

<label for="amount">Enter The amount of Package</label>

 <input type="text" class="form-control" name="amount" placeholder="amount" required >
 </div>
 <div class="form-group">

<label for="packageId">Enter The Package ID</label>

 <input type="text" class="form-control" name="packageId" placeholder="packageId" required >
 </div>
 <input type="submit" id="submit" class="btn btn-success" value="Add Package">
<input type="reset" class="btn btn-danger" value="Reset">
 
 </forms:form>
 </div>

</body>
</html>