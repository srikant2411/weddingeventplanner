<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vendor Registration</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<forms:form action="vendorregdata" method="post"  modelAttribute="vendordata">

<div class="form-group">
 <label for="vendorName">Enter the vendorName</label>
    <input type="text" class="form-control" name="vendorName" id="vendorName" placeholder="vendorName" required>
</div>
<div class="form-group">
    <label>Select Event Location</label>
<%--    <form:select path="location" name="location" items="${locationlist}"/><br><br>
 --%>   
 
 <select id="vendorType" name="vendorType">
    <option value="decoration">Decoration</option>
    <option value="catering">Catering</option>
    <option value="preshoot">Pre-Wedding Shoot</option>
    <option value="photo">Photo</option>
        <option value="video">video</option>
    
  </select>
 </div>
<!-- <div class="form-group">
 <label for="vendorType">Enter the vendorType</label>
    <input type="text" class="form-control" name="vendorType" id="vendorType" placeholder="vendorType" required>
</div> -->

<div class="form-group">
 <label for="phno">Enter the phno</label>
    <input type="text" class="form-control" name="phno" id="phno" placeholder="phno" required>
</div>

<div class="form-group">
 <label for="address">Enter the address</label>
    <input type="text" class="form-control" name="address" id="address" placeholder="address" required>
</div>
<div class="form-group">
    <input type="hidden" class="form-control" name="status" id="status" placeholder="status" value=1 required>
</div>
<!-- <div class="form-group">
 <label for="vendorId">Enter the vendorId</label>
    <input type="text" class="form-control" name="vendorId" id="vendorId" placeholder="vendorId" required>
</div> -->
<div class="form-group">
 <label for="password">Enter the password</label>
    <input type="text" class="form-control" name="password" id="password" placeholder="password" required>
</div>
<input type="submit" class="btn btn-success" value="Register">

</forms:form>

</body>
</html>