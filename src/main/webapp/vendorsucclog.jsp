<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vendor Home Page</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="vendorhome">Home</a>

<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="requestquot">View user</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="viewquot">Logout</a>

<h4>${mess}</h4>
${vednorId }
<table class="table table-striped table-bordered table-hover">
<caption>Your User Details are</caption>
<thead class="thead-dark">
<tr>
<th>User Id</th>

<th>User Name</th>
<th>From Date</th>
<th>To Date</th>

<th>Service Required</th>
<th>Mobile Number</th>
<th>Quotation Status</th>

</tr>
</thead>
<tbody>


<c:forEach items= "${eventlist}" var="eventlist">
<tr>
<td>${eventlist.userId}</td>

<td>${eventlist.username}</td>
<td>${eventlist.fromdate }</td>
<td>${eventlist.todate }</td>
<td>${eventlist.allServices }</td>
<td>${eventlist.mobileno }</td>
<td><a class="btn btn-success active" role="button" aria-pressed="true"  href="/event/vendorsendquo?vendorId=${vednorId}&userId=${eventlist.userId}">Send Quotation</a>
</td>
</tr>
</c:forEach>
</tbody>

</table>


</body>
</html>