<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Event</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
var mobileno="${regmobileno}";
var userno=$("#mobileno").val();
$("#submit").click(function(){
	if(mobileno!=userno){
		alert("Want to change Mobile number");
	}
		
});

});
</script>
</head>
<body>
<div class="container">
<forms:form action="insertevent" method="post"  modelAttribute="uevent">
<div class="form-group">

<label for="username">Your UserName is </label>

 <input type="text" class="form-control" value="${username}" disabled>
 <input type="hidden" class="form-control" name="username" placeholder="username" value="${username}" >
 </div>
<div class="form-group">

<label for="fromdate">Select The Event Start Date</label>
 <input type="date" class="form-control" name="fromdate" placeholder="fromdate" required>
 </div>
<div class="form-group">

<label for="todate">Select The Event End Date</label>
 <input type="date" class="form-control" name="todate" placeholder="todate" required>
 </div>
  <div class="form-group">
    <label>Select Event Location</label>
<%--    <form:select path="location" name="location" items="${locationlist}"/><br><br>
 --%>   
 
 <select id="location" name="location">
    <option value="Pune">Pune</option>
    <option value="Hyderabad">Hyderabad</option>
    <option value="Chennai">Chennai</option>
    <option value="Mumbai">Mumbai</option>
  </select>
 </div>
 <div class="form-group">

<label for="budget">Enter The Budget of event</label>
 <input type="number" class="form-control" name="budget" placeholder="budget" required>
 </div>
 <label>Services available</label><br>
<input type="checkbox" id="decoration" name="decoration" value="decoration">
<label for="decoration"> decoration</label><br>

<input type="checkbox" id="catering" name="catering" value="catering">
<label for="catering"> catering</label><br>

<input type="checkbox" id="preshoot" name="preshoot" value="preshoot">
<label for="preshoot"> Pre-Wedding Shoot</label><br>
<input type="checkbox" id="photo" name="photo" value="photo">
<label for="photo"> photo</label><br>
<input type="checkbox" id="video" name="video" value="video">
<label for="video"> video</label><br>

<div class="form-group">

<label for="mobileno"> Mobile Number</label>
 <input type="text" class="form-control" name="mobileno" id="mobileno" placeholder="mobileno" value="${regmobileno}" required>
 </div>
 <div class="form-group">


 <input type="hidden" class="form-control" name="userId" id="userId" placeholder="userId" value="${user.userId}" required>
 </div>
 
<input type="submit" id="submit" class="btn btn-success" value="Add Event">
<input type="reset" class="btn btn-danger" value="Reset">

</forms:form>

</div>


<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
	    <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>