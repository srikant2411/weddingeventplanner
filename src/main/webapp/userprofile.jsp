<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Profile</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="container-fluid">
<forms:form action="userupdate" method="post"  modelAttribute="userdata">

<div class="form-group">
 <label for="userId">UserId</label>
    <input type="text" class="form-control" name="userId" id="userId" placeholder="userId" value=${user.userId } disabled>
</div>
<div class="form-group">

    <input type="hidden" class="form-control" name="userId" id="userId" placeholder="userId" value=${user.userId } required>
</div>
<div class="form-group">
 <label for="firstName">Enter the firstName</label>
    <input type="text" class="form-control" name="firstName" id="firstName" value=${user.firstName } disabled>
</div>

<div class="form-group">
 <label for="lastName">Enter the lastName</label>
    <input type="text" class="form-control" name="lastName" id="lastName" value=${user.lastName } disabled>
</div>
<div class="form-group">

    <input type="hidden" class="form-control" name="firstName" id="firstName" value=${user.firstName } required>
</div>

<div class="form-group">

    <input type="hidden" class="form-control" name="lastName" id="lastName" value=${user.lastName } required>
</div>
<div class="form-group">
 <label for="dob">Enter the Date of Birth</label>
    <input type="date" class="form-control" name="dob" id="dob" value=${user.dob } required>
</div>
<div class="form-group">
 <label for="gender">Enter the Date of gender</label>
    <input type="text" class="form-control" name="gender" id="gender" value=${user.gender } required>
</div>
<div class="form-group">
 <label for="contactno">Enter the Date of contactno</label>
    <input type="text" class="form-control" name="contactno" id="contactno" value=${user.contactno } required>
</div>
<div class="form-group">
 <label for="password">Enter the Date of password</label>
    <input type="password" class="form-control" name="password" id="password" value=${user.password } required>
</div>
<div class="form-group">
 <label for="address">Enter the Date of address</label>
    <input type="text" class="form-control" name="address" id="address" value=${user.address } required>
</div>
<div class="form-group">

    <input type="hidden" class="form-control" name="status" id="status" placeholder="status" value=${user.status } required>
</div>
<input type="reset" class="btn btn-danger" value="Rest">

<input type="submit" class="btn btn-success" value="Update">
</forms:form> 
</div>

</body>
</html>