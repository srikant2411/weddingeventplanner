<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index Page</title>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>
    /*For Box styling*/
 #background{     
     background-position: center;
     background-size: cover;
     background-repeat: no-repeat;
     height: 400px;
     width: 800px;
     position: relative;

     margin: auto;
     width: 60%;
     border: 4px solid rgb(231, 114, 206);
     padding: 10px;
     border-radius: 10px;

    

     text-align:center;
     min-height: 10em;
     display: block;
     

     transition: 10s;
     animation-name: animate;
     animation-direction: alternate;
     animation-duration: 50s;
     animation-fill-mode:forwards ;
     animation-iteration-count: infinite;
     animation-play-state: running;
     animation-timing-function: ease-in-out;
 }
    /* For continouse annimation of pictures */
 @keyframes animate {     
     0%{
         background-image: url("https://wallpaperaccess.com/full/1315478.jpg");
     }
     35%{
         background-image: url("https://i.pinimg.com/originals/fa/13/b7/fa13b72a5671ba347101513313ce7417.jpg");
     }
     70%{
         background-image: url("https://prakashcreation.com/wp-content/uploads/2019/04/Chunda-Palace-867x422.jpg");
     }
     100%{
         background-image: url("https://images.unsplash.com/photo-1587271636175-90d58cdad458?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aW5kaWFuJTIwd2VkZGluZyUyMGRlY29yfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80");
     }
 } 
 /* Navigation bar Styling*/
 ul{
     list-style-type: none;
     margin: 0;
     padding: 0;
     overflow: hidden;
     background-color: rgb(32, 196, 32);
 }
 li{
     float: left;
 }
li a {
    display: block;
    color:white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover{
    background-color:palevioletred;
    color: white;
}
/* Background styling*/
body{
    background-image: url("https://www.wallpaperbetter.com/wallpaper/509/62/463/beautiful-wedding-rings-pictures-720P-wallpaper.jpg");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn1").click(function(){
            $("#popup-container").show();
        })
        $("#close-btn").click(function(){
            $("#popup-container").hide();
        })
    })
</script>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-secondary"></nav>
    <ul class="nav nav-tabs justify-content-center">
      <li class="nav-item"><a class="nav-link" id="btn1" href="admin">Admin Login</a></li>&nbsp
      <li class="nav-item"><a class="nav-link" id="btn1" href="vendorlogin">Vendor Login</a></li>
      <li class="nav-item"><a class="nav-link" id="btn1" href="userlogreg">User Login / Registration</a></li>
    </ul>
</header>
<body>
         <br>
         <div id="quta">
            <h1 style="font-size: 60px; text-align: center;  color: rgb(170, 3, 3); font-family: cursive;">&#128525 ... Your Wedding, Your Way ...&#128150</h1>
          </div>
          <br><br>
          <div id="background">
              <br><br><br><br><br>
                <h3 style="color: rgb(119, 30, 85); font-family: Times New Roman, Times, serif; font-weight: bolder;"> &nbsp &nbsp &nbsp &nbsp Find the best wedding vendors with thousands of trusted reviews...!</h3>
          </div>
           <div id="popup-container">
                <div id="close-btn">
                    <span id="close-btn">&#10060</span>
                </div>
                <img height="150px" src="https://images.all-free-download.com/images/graphicthumb/user_37223.jpg">
                <br>
                <input id="box" type="text" placeholder="Username">
                <br>
                <input id="box" type="password" placeholder="Password">
                <br>
                <input id="loginbtn" type="submit" value="Login">
            </div>

<!-- <a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="admin">Admin Login</a>

<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="vendorlogin">Vendor Login</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="userlogreg">User Login / Registration</a>
 -->
</body>
</html>