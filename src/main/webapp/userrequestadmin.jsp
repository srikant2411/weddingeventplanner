<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List Of Users</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<h1>List of all register users</h1>
<div class="container">
<table class="table table-striped table-bordered table-hover">
<caption>Your User Details are</caption>
<thead class="thead-dark">
<tr>
<th>User Name</th>
<th>From Date</th>
<th>To Date</th>

<th>Service Required</th>
<th>Mobile Number</th>

</tr>
</thead>
<tbody>


<c:forEach items= "${eventlist}" var="eventlist">
<tr>
<td>${eventlist.username}</td>
<td>${eventlist.fromdate }</td>
<td>${eventlist.todate }</td>
<td>${eventlist.allServices }</td>
<td>${eventlist.mobileno }</td>

</tr>
</c:forEach>
</tbody>

</table>
</div>
</body>
</html>