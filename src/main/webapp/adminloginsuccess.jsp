<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Home</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>



<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="usersqequestadmin">User Requests</a>

<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="addvendor">Add Vendor</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="userlogreg">Create Report</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="userlogreg">Gallery</a>
<a class="btn btn-primary btn-lg active" role="button" aria-pressed="true"  href="userlogreg">Log out</a>

<h1>List of all register users</h1>
<div class="container">
${vednorId }
<table class="table table-striped table-bordered table-hover">
<caption>Your User Details are</caption>
<thead class="thead-dark">
<tr>
<th>UserId</th>
<th>firstName</th>
<th>lastName</th>
<th>dob</th>
<th>gender</th>
<th>contactno</th>

<th>address</th>
<th>Approve status</th>
<th>Rejected status</th>
</tr>
</thead>
<tbody>


<c:forEach items= "${userlist}" var="userd">
<tr>
<td>${userd.userId}</td>
<td>${userd.firstName }</td>
<td>${userd.lastName }</td>
<td>${userd.dob }</td>
<td>${userd.gender }</td>
<td>${userd.contactno }</td>

<td>${userd.address }</td>
<td><a class="btn btn-success" role="button" href="/event/approveuser?userId=${userd.userId}">Approve Registration</a></td>

<td><a class="btn btn-danger" role="button" href="/event/rejectuser?userId=${userd.userId}">Reject Registration</a></td>

</tr>
</c:forEach>
</tbody>

</table>

</body>
</html>