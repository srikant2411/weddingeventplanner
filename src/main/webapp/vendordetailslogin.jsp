<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vendor Login</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
var num="${check}";
var mes="${message}";
console.log(num);

if(num==1){
	$("#username").css({"border":"2px solid red"});
	$("#password").css({"border":"2px solid red"});
	$("#mess").text(mes).css({"color":"red","font-weight":"Helvetica"});
}

$("#username").click(function(){
	$(this).css({"border":""});
	$("#password").css({"border":""});
	$("#mess").text("");
	num=0;
})

});
</script>
</head>
<body>
<forms:form action="vendorloginform" method="post"  modelAttribute="vendor">
<div class="form-group">

<label for="vendorId">Enter vendorId</label>
 <input type="text" class="form-control" name="vendorId" id="vendorId" placeholder="vendorId" required>
 </div>
 <div class="form-group">
 <label for="password">Enter The Password</label>
 <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
 </div>
<div id="mess">
</div> 
 
<div id="mess">
</div>

<input type="submit" class="btn btn-success" value="Login">


</forms:form>
</body>
</html>