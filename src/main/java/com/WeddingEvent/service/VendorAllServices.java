package com.WeddingEvent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.WeddingEvent.VendorDao.VendorRegistrationDao;
import com.WeddingEvent.model.QuotationDetails;
import com.WeddingEvent.model.VendorDetails;
import com.WeddingEvent.model.VendorPackage;

@Service
public class VendorAllServices {
	@Autowired
	VendorRegistrationDao vendordao;
	
	public boolean insertVendor(VendorDetails vendor) {
		return vendordao.vendorReg(vendor);
	}

	public VendorDetails VendoLogin(String vendorId,String password) {
		
		VendorDetails vendor=null;
		vendor= vendordao.vendorLogin(vendorId, password);
		return vendor;
		
	}
	public boolean vendorPasswordUpdate(String password, String newPassword) {
		return vendordao.vendorPasswordChange(password, newPassword);
	}
	public boolean vendorNewPackage(VendorPackage vpackage) {
		return vendordao.packageReg(vpackage);
	}
	public List<VendorPackage> displayAllPackages(int vendorId){
		return vendordao.packagebyVid(vendorId);
	}
	public boolean sendPackageUser(QuotationDetails quo) {
		return vendordao.sendPackage(quo);
	}
}
