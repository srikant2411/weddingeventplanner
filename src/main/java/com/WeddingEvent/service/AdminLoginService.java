package com.WeddingEvent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.WeddingEvent.AdminDao.AdminLoginDao;
import com.WeddingEvent.UserDao.UserRegistrationDao;
import com.WeddingEvent.model.AdminLogin;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;

@Service
public class AdminLoginService {

	@Autowired
	AdminLoginDao admindao;
	
	
	
	public AdminLogin adminAuthenticate(String username,String password) {
		AdminLogin admin=admindao.admin(username, password);
		if(admin!=null) {
			return admin;
		}
		else 
			return null;
		}
	public List<UserRegestraionDetails> displayAllUsers() {
		return admindao.findall();
	}
	public List<UserEvent> displayAllEvents(){
		return admindao.findAllEvent();
	}
	
	
	public void approveNewUser(String UserId) {
		admindao.approveUser(UserId);
	}
	public void rejectNewUser(String UserId) {
		admindao.rejectUser(UserId);
	}
}
