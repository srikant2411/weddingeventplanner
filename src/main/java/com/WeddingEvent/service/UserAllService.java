package com.WeddingEvent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.WeddingEvent.UserDao.UserRegistrationDao;
import com.WeddingEvent.UserEventDao.UserEventDAO;
import com.WeddingEvent.model.RecieveQuotation;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;

@Service
public class UserAllService {
	@Autowired
	UserRegistrationDao userdao;
	@Autowired
	UserEventDAO eventdao;
	public void insertNewUser(UserRegestraionDetails user) {
		userdao.userReg(user);
	}
	public boolean updateUserProfile(UserRegestraionDetails user) {
		return userdao.userUpdate(user);
	}
	public UserRegestraionDetails userLogin(String username,String password) {
		UserRegestraionDetails user=null;
	user=	userdao.userLogin(username, password);
	if(user!=null)
		return user;
	return	user;
	}
	
	public boolean insertUserEvent(UserEvent uevent) {
		boolean check= eventdao.eventInsert(uevent);
		return check;
	}
	public List<RecieveQuotation> recieveQuatation(String userId){
		return userdao.recieveQuotation(userId);
	}

}
