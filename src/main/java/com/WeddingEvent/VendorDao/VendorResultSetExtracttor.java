package com.WeddingEvent.VendorDao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.WeddingEvent.model.VendorDetails;

public class VendorResultSetExtracttor implements ResultSetExtractor<VendorDetails> {

	@Override
	public VendorDetails extractData(ResultSet rs) throws SQLException, DataAccessException {
		VendorDetails vendor=null;
		if(rs.next()) {
			vendor=new VendorDetails();
			vendor.setVendorName(rs.getString(1));
			vendor.setVendorType(rs.getString(2));
			vendor.setPhno(rs.getString(3));
			vendor.setAddress(rs.getString(4));
			vendor.setStatus(rs.getInt(5));
			vendor.setVendorId(rs.getInt(6));
			vendor.setPassword(rs.getString(7));
		}
		return vendor;
	}

}
