package com.WeddingEvent.VendorDao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.WeddingEvent.model.VendorPackage;

public class PackageDetailsRowMapper implements RowMapper<VendorPackage>{

	@Override
	public VendorPackage mapRow(ResultSet rs, int rowNum) throws SQLException {
		VendorPackage v= new VendorPackage();
		v.setVendorId(rs.getInt(1));
		v.setPackageName(rs.getString(2));
		v.setPackageDetails(rs.getString(3));
		v.setPhoto(rs.getString(4));
		v.setAmount(rs.getDouble(5));
		v.setPackageId(rs.getString(6));
		return v;
	}

}
