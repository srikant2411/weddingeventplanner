package com.WeddingEvent.VendorDao;

import java.util.List;

import com.WeddingEvent.model.QuotationDetails;
import com.WeddingEvent.model.VendorDetails;
import com.WeddingEvent.model.VendorPackage;

public interface VendorRegistrationDao {
public boolean vendorReg(VendorDetails vendor);
public VendorDetails vendorLogin(String vendorId,String password);
public boolean vendorPasswordChange(String password,String newPassword);
public boolean packageReg(VendorPackage vpackage);
public List<VendorPackage> packagebyVid(int vId);
public boolean sendPackage(QuotationDetails quo);
}
