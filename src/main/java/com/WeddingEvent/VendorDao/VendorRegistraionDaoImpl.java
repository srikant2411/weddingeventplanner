package com.WeddingEvent.VendorDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.WeddingEvent.model.QuotationDetails;
import com.WeddingEvent.model.VendorDetails;
import com.WeddingEvent.model.VendorPackage;

@Component
public class VendorRegistraionDaoImpl implements VendorRegistrationDao{

	@Autowired
	JdbcTemplate jdbctemplate;
	
	@Override
	public boolean vendorReg(VendorDetails vendor) {

		int res=jdbctemplate.update("insert into vendordetils values(?,?,?,?,?,?,?)",vendor.getVendorName(),vendor.getVendorType(),vendor.getPhno(),vendor.getAddress(),vendor.getStatus(),vendor.getVendorId(),vendor.getPassword());
		if(res>=1)
			return true;
		return false;
	}

	@Override
	public VendorDetails vendorLogin(String vendorId, String password) {
		return jdbctemplate.query("select * from vendordetils where vendorId=? and password=? ", new VendorResultSetExtracttor(),vendorId,password);
	}

	@Override
	public boolean vendorPasswordChange(String password, String newPassword) {
		int finalres=0;
		int res =jdbctemplate.update("update vendordetils set password=? where password=?",newPassword,password);
		if(res>=1) {
			finalres=jdbctemplate.update("update vendordetils set status=2 where password=?",newPassword);
		}
		if(finalres>=1) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean packageReg(VendorPackage vpackage) {
		int res=jdbctemplate.update("insert into vendorpackages values(?,?,?,?,?,?)",vpackage.getVendorId(),vpackage.getPackageName(),vpackage.getPackageDetails(),vpackage.getPhoto(),vpackage.getAmount(),vpackage.getPackageId());
		if(res>=1)
			return true;
		return false;
	}

	@Override
	public List<VendorPackage> packagebyVid(int vId) {
		return jdbctemplate.query("select * from vendorpackages where vendorId=?", new PackageDetailsRowMapper(),vId);
	}

	@Override
	public boolean sendPackage(QuotationDetails quo) {
		
		int res=jdbctemplate.update("insert into quotation values(?,?,?)",quo.getUserId(),quo.getVendorId(),quo.getPackageId());
		if(res>=1)
			return true;
		return false;
	}

}
