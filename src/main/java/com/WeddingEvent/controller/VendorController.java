package com.WeddingEvent.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.WeddingEvent.ExceptionHandler.InvalidUser;
import com.WeddingEvent.ExceptionHandler.InvalidVendor;
import com.WeddingEvent.ExceptionHandler.ProperDetails;
import com.WeddingEvent.model.QuotationDetails;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;
import com.WeddingEvent.model.VendorDetails;
import com.WeddingEvent.model.VendorPackage;
import com.WeddingEvent.service.AdminLoginService;
import com.WeddingEvent.service.VendorAllServices;

@Controller
public class VendorController {
	@Autowired
	VendorAllServices vendorServices;
	@Autowired
	AdminLoginService service;
	@RequestMapping(value = "/vendorregdata" , method = RequestMethod.POST)
	public String vendorinsert(@ModelAttribute("vendordata") VendorDetails vendordata,BindingResult result) {
		System.out.println(vendordata);
		try {
			vendorServices.insertVendor(vendordata);
			}catch(Exception e) {
				throw new ProperDetails("Enter Proper Details Please");
			}
		return "success";
	}
	
	@RequestMapping(value = "/vendorlogin", method= RequestMethod.GET)
	public String index() {
		return "vendordetailslogin";
	}
	@RequestMapping(value = "/addpackage", method= RequestMethod.GET)
	public String addVendorPackage() {
		return "addpackage";
	}
	@RequestMapping(value = "/vendorupdate" , method = RequestMethod.POST)
	public String VendorPasswordUpdate(Model model,@RequestParam String password,@RequestParam String newpassword) {
		
		boolean res=vendorServices.vendorPasswordUpdate(password, newpassword);
		if(res==true)
		return "vendordetailslogin";
		else
			return "vendorfirstlogin";
	}
	
	@RequestMapping(value = "/quotouser", method = RequestMethod.GET)
	public String sendQuotoUser(Model model, @RequestParam String userId,@RequestParam int vendorId,@RequestParam String packageId) {
		QuotationDetails quo=new QuotationDetails();
		quo.setUserId(userId);
		quo.setVendorId(vendorId);
		quo.setPackageId(packageId);
		System.out.println(quo);
		
		vendorServices.sendPackageUser(quo);

		List<VendorPackage> vpackage=vendorServices.displayAllPackages(vendorId);
		System.out.println(vpackage);
		model.addAttribute("vpackage",vpackage);
		return "packagedetailspage";
		

	}
	@RequestMapping(value = "/vendorsendquo", method = RequestMethod.GET)
	public String vendorPackageDetails(Model model, @RequestParam int vendorId,@RequestParam String userId,HttpSession session) {
		System.out.println(vendorId);
		System.out.println(userId);
		session.setAttribute("vendorId", vendorId);

		session.setAttribute("userId", userId);
		List<VendorPackage> vpackage=vendorServices.displayAllPackages(vendorId);
		System.out.println(vpackage);
		model.addAttribute("vpackage",vpackage);

//		List<UserRegestraionDetails> userlist = service.displayAllUsers();
//		System.out.println("From Controller"+userlist);
//		model.addAttribute("userlist", userlist);
		return "packagedetailspage";

	}
	@RequestMapping(value = "/insertpackage" , method = RequestMethod.POST)
	public String insertUserEvent(@ModelAttribute("upackage") VendorPackage upackage,BindingResult result,Model model) {
		System.out.println(upackage);
		
		boolean check=vendorServices.vendorNewPackage(upackage);
		if(check==true) {
			List<VendorPackage> vpackage=vendorServices.displayAllPackages(upackage.getVendorId());
			System.out.println(vpackage);
			model.addAttribute("vpackage",vpackage);
			return "packagedetailspage";
		}
		else
			return "index";
	}
	@RequestMapping(value = "/vendorloginform" , method = RequestMethod.POST)
	public String VendorLogin(Model model,@RequestParam String vendorId,@RequestParam String password,HttpSession session) {
		VendorDetails vendor=null;
		vendor= vendorServices.VendoLogin(vendorId, password);
		 if(vendor!=null) {
			
			if(vendor.getStatus()==1) {
				return "vendorfirstlogin";
			}
			session.setAttribute("mess",vendor.getVendorName() );
				System.out.println(vendor);
				
				  List<UserEvent> eventlist= service.displayAllEvents();
				  System.out.println("From Controller"+eventlist);
			  model.addAttribute("eventlist",eventlist);
			  System.out.println(vendor.getVendorId());
			  session.setAttribute("vednorId",vendor.getVendorId());
			//  model.addAttribute("vednorId",vendor.getVendorId());
//				 
				 return "vendorsucclog";
		
		 }
		
		 
		else  {
			throw new InvalidVendor("Incorrect User id or password");
		}
		
		
		
	}

}
