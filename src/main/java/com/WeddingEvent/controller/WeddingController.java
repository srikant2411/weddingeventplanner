package com.WeddingEvent.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.WeddingEvent.AdminDao.AdminLoginDao;
import com.WeddingEvent.ExceptionHandler.InvalidAdmin;
import com.WeddingEvent.ExceptionHandler.InvalidUser;
import com.WeddingEvent.ExceptionHandler.ProperDetails;
import com.WeddingEvent.model.AdminLogin;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;
import com.WeddingEvent.service.AdminLoginService;
import com.WeddingEvent.service.UserAllService;

@Controller
public class WeddingController {

	@Autowired
	AdminLoginService service;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin() {
		return "adminlogin";
	}

	@RequestMapping(value = "/userreg", method = RequestMethod.GET)
	public String userreg() {
		return "userregestration";
	}

	@RequestMapping(value = "/userlogreg", method = RequestMethod.GET)
	public String userlogreg() {
		return "userlogreg";
	}

	@RequestMapping(value = "/adminlogin", method = RequestMethod.POST)
	public String adminLoginHome(@ModelAttribute("admin") AdminLogin admin, BindingResult result, Model model) {
		System.out.println(admin);
		AdminLogin adminlogin = null;

		adminlogin = service.adminAuthenticate(admin.getUsername(), admin.getPassword());
		if (adminlogin != null) {

			List<UserRegestraionDetails> userlist = service.displayAllUsers();
			System.out.println(userlist);
			model.addAttribute("userlist", userlist);
			// return "userrequestadmin";
			return "adminloginsuccess";

		} else
			throw new InvalidAdmin("UserName or Password is Wrong");
	}

	@RequestMapping(value = "/addvendor", method = RequestMethod.GET)
	public String vendorReg() {
		return "vendorregestration";
	}
	
	@RequestMapping(value = "/usersqequestadmin", method = RequestMethod.GET)
	public String adminLogin(Model model) {

		
		  List<UserEvent> eventlist= service.displayAllEvents();
	  System.out.println("From Controller"+eventlist); 
		  model.addAttribute("eventlist",eventlist);
		  
		
		/*
		 * List<UserRegestraionDetails> userlist= admindao.findall();
		 * System.out.println(userlist); model.addAttribute("userlist",userlist);
		 */
		return "userrequestadmin";

	}

	@RequestMapping(value = "/approveuser", method = RequestMethod.GET)
	public String approveUser(Model model, @RequestParam String userId) {
		System.out.println(userId);
		service.approveNewUser(userId);

		List<UserRegestraionDetails> userlist = service.displayAllUsers();
		System.out.println("From Controller"+userlist);
		model.addAttribute("userlist", userlist);
		return "adminloginsuccess";

	}

	@RequestMapping(value = "/rejectuser", method = RequestMethod.GET)
	public String rejectUser(Model model, @RequestParam String userId) {
		System.out.println(userId);
		service.rejectNewUser(userId);

		List<UserRegestraionDetails> userlist = service.displayAllUsers();
		System.out.println(userlist);
		model.addAttribute("userlist", userlist);
		return "adminloginsuccess";

	}

}
