package com.WeddingEvent.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.WeddingEvent.ExceptionHandler.InvalidAdmin;
import com.WeddingEvent.ExceptionHandler.InvalidUser;
import com.WeddingEvent.ExceptionHandler.ProperDetails;
import com.WeddingEvent.model.RecieveQuotation;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;
import com.WeddingEvent.model.VendorDetails;
import com.WeddingEvent.service.UserAllService;
import com.WeddingEvent.service.VendorAllServices;

@Controller
public class UserController {
	@Autowired
	UserAllService userService;
	
	 @ModelAttribute("locationlist")
	  public List<String> buildState(){
	  List<String> serviceMap = new ArrayList<String>();
	  serviceMap.add("Pune");
	  serviceMap.add("Chennai");
	  serviceMap.add("Hyderbad");
	  return serviceMap;                     
	 } 
	 
	 @RequestMapping(value = "/viewquot" , method = RequestMethod.GET)
		public String recieveQuatation(Model model,@RequestParam String userId) {
		List<RecieveQuotation> rquo=userService.recieveQuatation(userId);
		
		System.out.println(rquo);
		model.addAttribute("rquo",rquo);
		 
		 return "viewquotation";
		}
		
	 
	@RequestMapping(value = "/requestquot" , method = RequestMethod.GET)
	public String AddEvent() {
		return "addevent";
	}
	@RequestMapping(value = "/userprofile" , method = RequestMethod.GET)
	public String displayUserProfile() {
		return "userprofile";
	}
	@RequestMapping(value = "/insertevent" , method = RequestMethod.POST)
	public String insertUserEvent(@ModelAttribute("uevent") UserEvent uevent,BindingResult result,Model model) {
		System.out.println(uevent);
		System.out.println(uevent.getUserId());

		boolean check=userService.insertUserEvent(uevent);
		if(check==true) {
			model.addAttribute("mess", "Request submitted successfully");
			return "usersucclog";
		}
		else
			return "index";
	}
	
	
	@RequestMapping(value = "/userregdata" , method = RequestMethod.POST)
	public String userinsert(@ModelAttribute("userdata") UserRegestraionDetails userdata,BindingResult result) {
		System.out.println(userdata);
		try {
			userService.insertNewUser(userdata);
			}catch(Exception e) {
				throw new ProperDetails("Enter Proper Details Please");
			}
		return "success";
	}
	@RequestMapping(value = "/userupdate" , method = RequestMethod.POST)
	public String userUpdate(@ModelAttribute("userdata") UserRegestraionDetails userdata,BindingResult result) {
		System.out.println(userdata);
		boolean res=userService.updateUserProfile(userdata);
		if(res==true)
			return "success";
//		try {
//			userService.insertNewUser(userdata);
//			}catch(Exception e) {
//				throw new ProperDetails("Enter Proper Details Please");
//			}
//		
		return "blankpage";
	}
	
	
	@RequestMapping(value = "/userlogin" , method = RequestMethod.POST)
	public String UserLogin(Model model,@RequestParam String username,@RequestParam String password,HttpSession session) {
		UserRegestraionDetails user=null;
		user= userService.userLogin(username, password);
		 if(user!=null) {
			
			 if(user.getStatus()==0) {
				 throw new InvalidUser("User not Register");
			 }
			 if(user.getStatus()==1) {
				 return "blankpage";
			 }
			 else {
				
				 session.putValue("user",user);
				 session.setAttribute("username", user.getFirstName()+" "+user.getLastName());
				 session.setAttribute("regmobileno", user.getContactno());
				 return "usersucclog";
		}
		 }
		
		 
		else  {
			throw new InvalidUser("Incorrect User id or password");
		}
		
		
		
	}
}
