package com.WeddingEvent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeddingEventPlannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeddingEventPlannerApplication.class, args);
	}

}
