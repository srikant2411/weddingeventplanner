package com.WeddingEvent.UserDao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.WeddingEvent.model.RecieveQuotation;

public class RecieveQuotRowMapper implements RowMapper<RecieveQuotation>{

	@Override
	public RecieveQuotation mapRow(ResultSet rs, int rowNum) throws SQLException {
		RecieveQuotation rq=new RecieveQuotation();
		rq.setUserId(rs.getString(1));
		rq.setVendorId(rs.getInt(2));
		rq.setPackageId(rs.getString(3));
		rq.setVendorName(rs.getString(4));
		rq.setVendorType(rs.getString(5));
		rq.setPhno(rs.getString(6));
		rq.setPackageName(rs.getString(7));
		rq.setPackageDetails(rs.getString(8));
		rq.setPhoto(rs.getString(9));
		rq.setAmount(rs.getDouble(10));
		return rq;
	}

}
