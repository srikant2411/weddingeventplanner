package com.WeddingEvent.UserDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.WeddingEvent.ExceptionHandler.ProperDetails;
import com.WeddingEvent.model.QuotationDetails;
import com.WeddingEvent.model.RecieveQuotation;
import com.WeddingEvent.model.UserRegestraionDetails;
import com.WeddingEvent.model.VendorDetails;

@Component
public class UserRegestraionDaoImpl implements UserRegistrationDao{

	@Autowired
	JdbcTemplate jdbctemplate;
	@Override
	public boolean userReg(UserRegestraionDetails user) {
		int res=0;
		try {
			String phno=user.getContactno().substring(7);
			String userId=user.getFirstName()+phno;
			user.setUserId(userId);
			res=jdbctemplate.update("insert into userdetails values(?,?,?,?,?,?,?,?,?)",user.getUserId(),user.getFirstName(),user.getLastName(),user.getDob(),user.getGender(),user.getContactno(),user.getPassword(),user.getAddress(),user.getStatus());
			
		}catch(Exception e) {
			throw new ProperDetails("Enter Proper Details");
		}
		if(res==0) {
			throw new ProperDetails("Not a valid name");
			
			
		}
		if(res>0) {
			return true;
		}
		return false;
	}
	@Override
	public UserRegestraionDetails userLogin(String username, String password) {
		
		return jdbctemplate.query("select *  from userdetails where userId=? and password=?", new UserResultSetExtractor(), username,password);
	}
	@Override
	public boolean userUpdate(UserRegestraionDetails user) {
		int res= jdbctemplate.update("update userdetails set dob=?, gender=?,contactno=?,password=?,address=? where userId=?", user.getDob(),user.getGender(),user.getContactno(),user.getPassword(),user.getAddress(),user.getUserId());
		if(res>=1)
			return true;
		return false;
	}
	/*
	 * @Override public boolean reciveQuotation(String userId) { jdbctemplate.
	 * query("select q.packageId,v.vendorName,v.phno from quotation q join vendordetils v on q.vendorId=v.vendorId where q.userId=?"
	 * , userId); return false; }
	 */
	@Override
	public List<RecieveQuotation> recieveQuotation(String userId) {
		return jdbctemplate.query("select q.userId,q.vendorId,q.packageId,v.vendorName,v.vendorType,v.phno , p.packageName, p.packageDetails, p.photo,p.amount from quotation q join vendordetils v on q.vendorId=v.vendorId join vendorpackages p on q.packageId= p.packageId where q.userId=? group by (q.packageId)", new RecieveQuotRowMapper(),userId);
	}
	

}
