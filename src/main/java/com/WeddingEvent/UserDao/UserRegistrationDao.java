package com.WeddingEvent.UserDao;

import java.util.List;

import com.WeddingEvent.model.QuotationDetails;
import com.WeddingEvent.model.RecieveQuotation;
import com.WeddingEvent.model.UserRegestraionDetails;
import com.WeddingEvent.model.VendorDetails;

public interface UserRegistrationDao {
	
	public boolean userReg(UserRegestraionDetails user);
	public UserRegestraionDetails userLogin(String username,String password);
	public boolean userUpdate(UserRegestraionDetails user);
//	public boolean reciveQuotation(String userId);
	public List<RecieveQuotation> recieveQuotation(String userId);
	
}
