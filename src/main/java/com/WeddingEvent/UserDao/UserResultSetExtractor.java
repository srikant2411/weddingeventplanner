package com.WeddingEvent.UserDao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.WeddingEvent.model.UserRegestraionDetails;

public class UserResultSetExtractor implements ResultSetExtractor<UserRegestraionDetails> {

	@Override
	public UserRegestraionDetails extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		UserRegestraionDetails user=null;
		if(rs.next()) {
			user=new UserRegestraionDetails();
			user.setUserId(rs.getString(1));
			user.setFirstName(rs.getString(2));
			user.setLastName(rs.getString(3));
			user.setDob(rs.getDate(4));
			user.setGender(rs.getString(5));
			user.setContactno(rs.getString(6));
			user.setPassword(rs.getString(7));
			user.setAddress(rs.getString(8));
			user.setStatus(rs.getInt(9));
			
		}
		return user;
	}
	

}
