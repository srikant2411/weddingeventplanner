package com.WeddingEvent.ExceptionHandler;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;




@ControllerAdvice
public class ExceptionHandlingController {
	
	@ExceptionHandler(InvalidAdmin.class)
	public String duplicateResouse(InvalidAdmin ex, Model model) {
		String mes = ex.getMessage();
		model.addAttribute("message", mes);
		model.addAttribute("check",1);
	
		return "adminlogin";
	}
	@ExceptionHandler(InvalidUser.class)
	public String invalidUser(InvalidUser ex, Model model) {
		String mes = ex.getMessage();
		model.addAttribute("message", mes);
		model.addAttribute("check",1);
	
		return "userlogreg";
	}
	@ExceptionHandler(InvalidVendor.class)
	public String invalidvendor(InvalidVendor ex, Model model) {
		String mes = ex.getMessage();
		model.addAttribute("message", mes);
		model.addAttribute("check",1);
	
		return "vendordetailslogin";
	}
	@ExceptionHandler(ProperDetails.class)
	public String notAValidInput(ProperDetails pr,Model model) {
		String mes= pr.getMessage();
		model.addAttribute(mes);
		
		return "index";
	}
	

}
