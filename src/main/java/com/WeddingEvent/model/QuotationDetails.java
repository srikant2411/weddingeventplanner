package com.WeddingEvent.model;

public class QuotationDetails {
	private String userId;
	private int vendorId;
	private String packageId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	@Override
	public String toString() {
		return "QuotationDetails [userId=" + userId + ", vendorId=" + vendorId + ", packageId=" + packageId + "]";
	}
	

}
