package com.WeddingEvent.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class UserEvent {
	private String username;
	@DateTimeFormat(pattern = "yyyy-mm-dd")
	private Date fromdate;
	@DateTimeFormat(pattern = "yyyy-mm-dd")
	private Date todate;
	private String location;
	private Double budget;
	private String allServices;
	
	private String decoration;
	private String catering;
	private String preshoot;
	private String photo;
	private String video;
	private String mobileno;
	private String userId;
	
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAllServices() {
		return allServices;
	}
	public void setAllServices(String allServices) {
		this.allServices = allServices;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getFromdate() {
		return fromdate;
	}
	public void setFromdate(Date fromdate) {
		this.fromdate = fromdate;
	}
	public Date getTodate() {
		return todate;
	}
	public void setTodate(Date todate) {
		this.todate = todate;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Double getBudget() {
		return budget;
	}
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	public String getDecoration() {
		return decoration;
	}
	public void setDecoration(String decoration) {
		this.decoration = decoration;
	}
	public String getCatering() {
		return catering;
	}
	public void setCatering(String catering) {
		this.catering = catering;
	}
	public String getPreshoot() {
		return preshoot;
	}
	public void setPreshoot(String preshoot) {
		this.preshoot = preshoot;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	@Override
	public String toString() {
		return "UserEvent [username=" + username + ", fromdate=" + fromdate + ", todate=" + todate + ", location="
				+ location + ", budget=" + budget + ", allServices=" + allServices + ", decoration=" + decoration
				+ ", catering=" + catering + ", preshoot=" + preshoot + ", photo=" + photo + ", video=" + video
				+ ", mobileno=" + mobileno + ", userId=" + userId + "]";
	}
	
	
	
}
