package com.WeddingEvent.model;

public class VendorPackage {
	private int vendorId;
	private String packageName;
	private String packageDetails;
	private String photo;
	private double amount;
	private String packageId;
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageDetails() {
		return packageDetails;
	}
	public void setPackageDetails(String packageDetails) {
		this.packageDetails = packageDetails;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	@Override
	public String toString() {
		return "VendorPackage [vendorId=" + vendorId + ", packageName=" + packageName + ", packageDetails="
				+ packageDetails + ", photo=" + photo + ", amount=" + amount + ", packageId=" + packageId + "]";
	}
	

}
