package com.WeddingEvent.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class UserRegestraionDetails {
	
	private String userId;
	private String firstName;
	private String lastName;
	@DateTimeFormat(pattern = "yyyy-mm-dd")
	private Date dob;
	private String gender;
	private String contactno;
	
	private String password;
	private String address;
	private int status;
	
	
	
	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Date getDob() {
		return dob;
	}



	public void setDob(Date dob) {
		this.dob = dob;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public String getContactno() {
		return contactno;
	}



	public void setContactno(String contactno) {
		this.contactno = contactno;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	@Override
	public String toString() {
		return "UserRegestraionDetails [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", dob=" + dob + ", gender=" + gender + ", contactno=" + contactno + ", password=" + password
				+ ", address=" + address + ", status=" + status + "]";
	}
	
	
	
	
	

}
