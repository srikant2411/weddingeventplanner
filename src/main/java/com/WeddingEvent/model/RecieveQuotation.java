package com.WeddingEvent.model;

public class RecieveQuotation {
	private String userId;
	private int vendorId;
	private String packageId;
	private String vendorName;
	private String vendorType;
	private String phno;
	private String packageName;
	private String packageDetails;
	private String photo;
	private double amount;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorType() {
		return vendorType;
	}
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
	public String getPhno() {
		return phno;
	}
	public void setPhno(String phno) {
		this.phno = phno;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageDetails() {
		return packageDetails;
	}
	public void setPackageDetails(String packageDetails) {
		this.packageDetails = packageDetails;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "RecieveQuotation [userId=" + userId + ", vendorId=" + vendorId + ", packageId=" + packageId
				+ ", vendorName=" + vendorName + ", vendorType=" + vendorType + ", phno=" + phno + ", packageName="
				+ packageName + ", packageDetails=" + packageDetails + ", photo=" + photo + ", amount=" + amount + "]";
	}
	
	
}
