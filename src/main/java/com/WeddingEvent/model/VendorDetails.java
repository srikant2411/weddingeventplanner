package com.WeddingEvent.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.springframework.data.annotation.Id;

@Entity
public class VendorDetails {
	private String vendorName;
	private String vendorType;
	private String phno;
	private String address;
	private int status;
	@javax.persistence.Id @GeneratedValue(strategy = GenerationType.AUTO)
	private int vendorId;
	private String password;
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public String getVendorType() {
		return vendorType;
	}
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
	public String getPhno() {
		return phno;
	}
	public void setPhno(String phno) {
		this.phno = phno;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "VendorDetails [vendorName=" + vendorName + ", VendorType=" + vendorType + ", phno=" + phno
				+ ", address=" + address + ", status=" + status + ", vendorId=" + vendorId + ", password=" + password
				+ "]";
	}
	

}
