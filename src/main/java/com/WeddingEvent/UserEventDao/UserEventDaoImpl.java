package com.WeddingEvent.UserEventDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.WeddingEvent.model.UserEvent;

@Component
public class UserEventDaoImpl implements UserEventDAO{
	
	@Autowired
	JdbcTemplate jdbctemplate;
	public String services="";

	@Override
	public boolean eventInsert(UserEvent uevent) {
		int res=0;
		
		if(uevent.getDecoration()!=null) {
			services+=uevent.getDecoration()+" ,";
		}
		
		if(uevent.getCatering()!=null) {
			services+=uevent.getCatering()+" ,";
		}
		if(uevent.getPreshoot()!=null) {
			services+=uevent.getPreshoot()+" ,";
		}
		if(uevent.getVideo()!=null) {
			services+=uevent.getVideo()+" ,";
		}
		if(uevent.getPhoto()!=null) {
			services+=uevent.getPhoto()+" ,";
		}
		uevent.setAllServices(services);
		res= jdbctemplate.update("insert into userevent values(?,?,?,?,?,?,?,?)",uevent.getUsername(),uevent.getFromdate(),uevent.getTodate(),uevent.getLocation(),uevent.getBudget(),uevent.getAllServices(),uevent.getMobileno(),uevent.getUserId());
		if(res>=1)
			return true;
		else
			return false;
	}

}
