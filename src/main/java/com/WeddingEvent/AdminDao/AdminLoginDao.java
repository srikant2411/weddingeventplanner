package com.WeddingEvent.AdminDao;

import java.util.List;

import com.WeddingEvent.model.AdminLogin;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;

public interface AdminLoginDao {
public AdminLogin admin(String username,String password);
public List<UserRegestraionDetails> findall();
public boolean approveUser(String userId);
public boolean rejectUser(String userId);
public List<UserEvent> findAllEvent();
}
