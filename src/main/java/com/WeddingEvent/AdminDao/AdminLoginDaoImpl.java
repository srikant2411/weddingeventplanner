package com.WeddingEvent.AdminDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.WeddingEvent.model.AdminLogin;
import com.WeddingEvent.model.UserEvent;
import com.WeddingEvent.model.UserRegestraionDetails;
@Component
public class AdminLoginDaoImpl implements AdminLoginDao {
	@Autowired
	JdbcTemplate jdbctemplate;

	@Override
	public AdminLogin admin(String username, String password) {
		
		return jdbctemplate.query("select * from adminlogin where username=? and password=?", new AdminLoginResultSetExtractor(), username,password);
	}

	@Override
	public List<UserRegestraionDetails> findall() {
		return jdbctemplate.query("select * from userdetails", new UserDeailsRowMapper());
	}

	@Override
	public boolean approveUser(String userId) {
		int res = jdbctemplate.update("update userdetails set status=2 where userId=?",userId);
		if(res>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean rejectUser(String userId) {
		int res = jdbctemplate.update("update userdetails set status=0 where userId=?",userId);
		if(res>0) {
			return true;
		}
		return false;
	}

	@Override
	public List<UserEvent> findAllEvent() {

		return jdbctemplate.query("select * from userevent", new EventDetailsRowMapper());
	}

}
