package com.WeddingEvent.AdminDao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.WeddingEvent.model.UserEvent;

public class EventDetailsRowMapper implements RowMapper<UserEvent>{

	@Override
	public UserEvent mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserEvent uevent= new UserEvent();
		uevent.setUsername(rs.getString(1));
		uevent.setFromdate(rs.getDate(2));
		uevent.setTodate(rs.getDate(3));
		uevent.setLocation(rs.getString(4));
		uevent.setBudget(rs.getDouble(5));
		uevent.setDecoration("no");
		uevent.setCatering("no");
		uevent.setPreshoot("no");
		uevent.setPhoto("no");
		uevent.setVideo("no");
		uevent.setAllServices(rs.getString(6));
		uevent.setMobileno(rs.getString(7));
		uevent.setUserId(rs.getString(8));
		
		
		return uevent;
	}

}
