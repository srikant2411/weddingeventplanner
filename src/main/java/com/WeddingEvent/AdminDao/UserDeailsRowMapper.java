package com.WeddingEvent.AdminDao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.WeddingEvent.model.UserRegestraionDetails;

public class UserDeailsRowMapper implements RowMapper<UserRegestraionDetails> {

	@Override
	public UserRegestraionDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserRegestraionDetails user= new UserRegestraionDetails();
		user.setUserId(rs.getString(1));
		user.setFirstName(rs.getString(2));
		user.setLastName(rs.getString(3));
		user.setDob(rs.getDate(4));
		user.setGender(rs.getString(5));
		user.setContactno(rs.getString(6));
		user.setPassword(rs.getString(7));
		user.setAddress(rs.getString(8));
		user.setStatus(rs.getInt(9));
		return user;
	}

}
